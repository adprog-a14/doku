package com.doku.comment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "Comment")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_comment", updatable = false)
    private int idComment;

    @Column(name = "word", nullable = false)
    private String word;

    @Column(name = "vote", nullable = false)
    private int vote;

    @Column(name = "description", nullable = false)
    private String description;



    public Comment(String description, String word){
        this.description = description;
        this.word = word;
    }

    public String getDescription (){
        return description;
    }
    public int getVote(){
        return vote;
    }
    public int getIdComment(){return idComment;}
    public String getWord(){
        return word;
    }
}
