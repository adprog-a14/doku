package com.doku.comment.repository;

import com.doku.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Comment findById(int idComment);
    List<Comment> findAllByWordOrderByVoteDesc(String word);
}

