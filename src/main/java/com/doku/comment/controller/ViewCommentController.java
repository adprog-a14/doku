package com.doku.comment.controller;

import com.doku.comment.model.Comment;
import com.doku.comment.service.CommentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ViewCommentController {

    @Autowired
    private CommentServiceImpl commentService;

    @PostMapping(path = "/input-comment/{word}", produces = { "application/json" })
    @ResponseStatus(value = HttpStatus.OK)
    public void inputForm(@RequestBody String description, @PathVariable(value = "word") String word) {
        Comment comment = commentService.newComment(description,word);
        commentService.createComment(comment);
    }

    @PostMapping(path = "/vote-up/{idComment}", produces = { "application/json" })
    @ResponseStatus(value = HttpStatus.OK)
    public void voteUp(@PathVariable(value = "idComment") int idComment) {
        commentService.voteUp(idComment);
    }

    @PostMapping(path = "/vote-down/{idComment}", produces = { "application/json" })
    @ResponseStatus(value = HttpStatus.OK)
    public void voteDown(@PathVariable(value = "idComment") int idComment) {
        commentService.voteDown(idComment);
    }

    @GetMapping(path = "/getCommentList/{word}", produces = { "application/json" })
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity getComment(@PathVariable(value = "word") String word) {
        return ResponseEntity.ok( commentService.getListComment(word));
    }

}
