package com.doku.comment.service;


import com.doku.comment.model.Comment;
import com.doku.comment.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Comment createComment(Comment comment) {
        if (commentRepository.findById(comment.getIdComment()) == null) {
            commentRepository.save(comment);
            return comment;
        }
        return null;
    }

    @Override
    public Iterable<Comment> getListComment(String word) {
        return commentRepository.findAllByWordOrderByVoteDesc(word);
    }

    @Override
    public void voteUp(int idComment){
        Comment existingComment = commentRepository.findById(idComment);
        if (existingComment != null) {
            existingComment.setVote(existingComment.getVote() + 1);
            commentRepository.save(existingComment);
        }
    }
    @Override
    public void voteDown(int idComment){
        Comment existingComment = commentRepository.findById(idComment);
        if (existingComment != null) {
            existingComment.setVote(existingComment.getVote() - 1);
            commentRepository.save(existingComment);
        }
    }

    @Override
    public Comment newComment(String description, String word) {
        Comment comment = new Comment(description, word);
        return comment;
    }
}
