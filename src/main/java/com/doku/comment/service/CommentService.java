package com.doku.comment.service;
import com.doku.comment.model.Comment;

public interface CommentService {
    Comment createComment(Comment comment);

    Iterable<Comment> getListComment(String word);

    void voteUp(int idComment);

    void voteDown(int idComment);

    Comment newComment(String description, String word);
}

