package com.doku.security;

import com.doku.auth.UserRepository;
import com.doku.auth.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SecurityController {

    @Autowired
    private UserRepository userRepository;

    /**
     * Ini untuk ke login page.
     * @return login page
     */
    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    @GetMapping("/registration")
    public String registrationPage() {
        return "registration";
    }

    /**
     * Berfungsi untuk membuat user baru yang telah sign-in.
     * @param username username akun baru
     * @param password password akun baru
     * @return
     */
    @PostMapping("/new-registration")
    public String newRegistration(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    ) {
        Users newUsers = new Users(username, password);
        userRepository.save(newUsers);
        return "redirect:/login";
    }
}
