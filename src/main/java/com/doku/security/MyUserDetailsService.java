package com.doku.security;

import com.doku.auth.UserRepository;
import com.doku.auth.model.MyUserDetails;
import com.doku.auth.model.Users;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Melakukan load user kita login terjadi dengan menggunakan username.
     * @param username username user
     * @return Objek MyUserDetails
     * @throws UsernameNotFoundException jika tidak ditemukan username tersebut
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> user = userRepository.findUserByUsername(username);
        user.orElseThrow(() -> new UsernameNotFoundException("Not Found: " + username));
        return user.map(MyUserDetails::new).get();
    }


}
