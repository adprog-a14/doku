package com.doku.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Menentukan UserDetailsService mana yang akan digunakan.
     * @param auth AuthenticationManagerBuilder
     * @throws Exception jika terjadi error
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    /**
     * Melakukan konfigurasi apakah boleh mengakses suatu URL atau tidak.
     * @param http HttpSecurity
     * @throws Exception Jika terjadi error
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()

                .antMatchers("/registration", "/new-registration",
                        "/css/**", "/fonts/**",
                        "/images/**", "/js/**", "/vendor/**").permitAll()
                .antMatchers(HttpMethod.POST, "/translate").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()


                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll().and().csrf().disable();
    }

    /**
     * Password encoder tidak menggunakan enkripsi agar mempermudah development.
     * @return NoOpPasswordEncoder
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
