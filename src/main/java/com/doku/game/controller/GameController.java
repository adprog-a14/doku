package com.doku.game.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;

@Controller
public class GameController {
    @GetMapping("/game")
    public String homepageGame() {
        return "game/GameHomepage";
    }

    @GetMapping("/gameLanguage/{language}")
    public String startGame(Model model , @PathVariable(value = "language") String language) {
        model.addAttribute("languages",language);
        return "game/StartGame";

    }
}
