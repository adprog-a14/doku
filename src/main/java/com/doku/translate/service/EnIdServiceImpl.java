package com.doku.translate.service;

import com.doku.translate.model.EnId;
import com.doku.translate.model.TransFacade;
import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnIdServiceImpl implements EnIdService {
    @Autowired
    private EnIdRepository enIdRepository;

    @Autowired
    private TransFacade transFacade;

    @Override
    public void addWord(String word, String trans) {
        enIdRepository.save(new EnId(word, trans));
    }

    @Override
    public String[] getTransbyWord(String word) {
        String[] result = transFacade.translate(word);

        return result;
    }
}
