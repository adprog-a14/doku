package com.doku.translate.service;

public interface EnIdService {
    void addWord(String word, String trans);

    String[] getTransbyWord(String word);
}
