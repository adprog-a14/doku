package com.doku.translate.repository;

import com.doku.translate.model.EnId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnIdRepository extends JpaRepository<EnId, String> {
    EnId findByWord(String word);
}
