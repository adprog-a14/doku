package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LihParserAwal implements Parser {
    @Autowired
    EnIdRepository enIdRepository;

    @Override
    public String[] parse(String trans) {
        EnId theWord = enIdRepository.findByWord(trans.replace("lih ", "")
                .replaceAll("[^a-zA-Z]", "").toLowerCase());

        String res = theWord.getTrans();
        String note = ", irregular form of " + "\"" + theWord.getWord() + "\"";

        return new String[]{"", res, note};
    }

    public String toString() {
        return "LihParserAwal";
    }
}
