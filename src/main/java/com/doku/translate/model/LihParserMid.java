package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LihParserMid implements Parser {
    @Autowired
    EnIdRepository enIdRepository;

    @Override
    public String[] parse(String trans) {
        trans = trans.replace("lih ", "");
        String real = trans.substring(trans.lastIndexOf(' ') + 1, trans.length() - 1).toLowerCase();
        String note = ", irregular form of " + "\"" + real + "\"";
        String result = trans
                .replace(real.toUpperCase(), enIdRepository
                .findByWord(real.replaceAll("[^a-zA-Z]", "").toLowerCase()).getTrans());
        return new String[]{"", result, note};
    }

    public String toString() {
        return "LihParserMid";
    }
}
