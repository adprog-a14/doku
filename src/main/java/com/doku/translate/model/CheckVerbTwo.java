package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckVerbTwo implements Parser {
    @Autowired
    EnIdRepository enIdRepository;

    @Override
    public String[] parse(String word) {
        String cutLastThree = word.substring(0, word.length() - 3);

        EnId minone = enIdRepository.findByWord(word.substring(0, word.length() - 1));
        EnId mintwo = enIdRepository.findByWord(word.substring(0, word.length() - 2));
        EnId minthree = enIdRepository.findByWord(cutLastThree);
        EnId ied = enIdRepository.findByWord(cutLastThree + "y");

        EnId[] possiblities = {minone, mintwo, ied, minthree};

        for (EnId poss : possiblities) {
            if (poss != null) {
                return new String[]{"", poss.getTrans(),
                    ", past participle form of " + "\"" + poss.getWord() + "\""};
            }
        }
        return new String[]{"", "Kata tidak dapat ditemukan di kamus", ""};
    }

    public String toString() {
        return "CheckVerbTwoParser";
    }
}
