package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckAdverb implements Parser {
    @Autowired
    EnIdRepository enIdRepository;

    @Override
    public String[] parse(String word) {
        EnId minone = enIdRepository.findByWord(word.substring(0, word.length() - 1) + "e");
        EnId mintwo = enIdRepository.findByWord(word.substring(0, word.length() - 2));
        EnId minthree = enIdRepository.findByWord(word.substring(0, word.length() - 3) + "y");
        EnId minfour = enIdRepository.findByWord(word.substring(0, word.length() - 4));

        EnId[] possiblities = {minone, mintwo, minthree, minfour};

        for (EnId possibility : possiblities) {
            if (possibility != null) {
                return new String[]{"", possibility.getTrans(),
                    ", adverb form of " + "\"" + possibility.getWord() + "\""};
            }
        }
        return new String[]{"", "Kata tidak dapat ditemukan di kamus", ""};
    }

    public String toString() {
        return "CheckAdverbParser";
    }
}
