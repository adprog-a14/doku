package com.doku.translate.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ParserFactory {

    private final Map<String, Parser> parserMap = new HashMap<>();

    @Autowired
    ApplicationContext context;

    @PostConstruct
    public void initialize() {
        populateDataMapperMap(context.getBeansOfType(Parser.class).values().iterator());
    }

    private void populateDataMapperMap(final Iterator<Parser> classIterator) {
        while (classIterator.hasNext()) {
            Parser parser = classIterator.next();
            String name = parser.getClass().getName();
            parserMap.put(name.substring(name.lastIndexOf('.') + 1).trim(), parser);
        }
    }

    public Parser getParser(String parser) {
        return parserMap.get(parser);
    }
}