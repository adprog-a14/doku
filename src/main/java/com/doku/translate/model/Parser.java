package com.doku.translate.model;

public interface Parser {
    String[] parse(String word);

    String toString();
}
