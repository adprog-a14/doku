package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransFacade {
    @Autowired
    EnIdRepository enIdRepository;

    @Autowired
    ParserFactory parserFactory;

    @Autowired
    ParseStrategy parseStrategy;

    public TransFacade() {

    }

    /**
     * method to translate word.
     * @summary method to translate word
     * @param word the English word
     * @return array of string containing the word, the translation, and note
     *         regarding the word form
     */
    public String[] translate(String word) {
        parserFactory.initialize();
        word = word.replaceAll("[^a-zA-Z]", "").toLowerCase();
        EnId enId = enIdRepository.findByWord(word);
        String[] result;

        if (enId != null) {
            result = lihParse(word, enId.getTrans(), enId);
        } else {
            result = regParse(word);
        }
        result[0] = word;

        return result;
    }

    private String[] regParse(String word) {
        if (word.endsWith("ed")) {
            parseStrategy.setParser(parserFactory.getParser("CheckVerbTwo"));
        } else if (word.endsWith("s")) {
            parseStrategy.setParser(parserFactory.getParser("CheckPlural"));
        } else if (word.endsWith("ly")) {
            parseStrategy.setParser(parserFactory.getParser("CheckAdverb"));
        } else if (word.endsWith("ing")) {
            parseStrategy.setParser(parserFactory.getParser("CheckIng"));
        } else if (word.endsWith("er") || word.endsWith("est")) {
            parseStrategy.setParser(parserFactory.getParser("CheckLative"));
        } else {
            return new String[] { word, "Kata tidak dapat ditemukan di kamus", "" };
        }

        String[] result = parseStrategy.parse(word);
        saveWord(word, result[1], result[2]);
        return result;
    }

    private String[] lihParse(String word, String trans, EnId enId) {
        if (trans.startsWith("lih ")) {
            parseStrategy.setParser(parserFactory.getParser("LihParserAwal"));
        } else if (trans.contains(" lih ")) {
            parseStrategy.setParser(parserFactory.getParser("LihParserMid"));
        } else {
            return new String[] { word, trans, enId.getNote() };
        }

        String[] result = parseStrategy.parse(trans);
        saveWord(enId, word, result[1], result[2]);
        return result;
    }

    private void saveWord(EnId enId, String word, String result, String note) {
        if (!result.equals(enId.getTrans())) {
            enIdRepository.delete(enId);
            enIdRepository.save(new EnId(word, result, note));
        }
    }

    private void saveWord(String word, String result, String note) {
        if (!result.equals("Kata tidak dapat ditemukan di kamus")) {
            enIdRepository.save(new EnId(word, result, note));
        }
    }
}
