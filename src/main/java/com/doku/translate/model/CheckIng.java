package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckIng implements Parser {
    @Autowired
    EnIdRepository enIdRepository;

    @Override
    public String[] parse(String word) {
        String cutLastThree = word.substring(0, word.length() - 3);
        String cutLastFour = word.substring(0, word.length() - 4);

        EnId minIng = enIdRepository.findByWord(cutLastThree);
        EnId plusE = enIdRepository.findByWord(cutLastThree + "e");
        EnId plusIe = enIdRepository.findByWord(cutLastFour + "ie");
        EnId minFour = enIdRepository.findByWord(cutLastFour);

        EnId[] possiblities = {minIng, plusE, plusIe, minFour};

        for (EnId poss : possiblities) {
            if (poss != null) {
                return new String[]{"", poss.getTrans(),
                    ", present participle or gerund form of " + "\"" + poss.getWord() + "\""};
            }
        }
        return new String[]{"", "Kata tidak dapat ditemukan di kamus", ""};
    }

    public String toString() {
        return "CheckIngParser";
    }
}
