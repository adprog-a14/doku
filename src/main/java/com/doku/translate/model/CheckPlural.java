package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckPlural implements Parser {
    @Autowired
    EnIdRepository enIdRepository;

    @Override
    public String[] parse(String word) {
        String cutLastThree = word.substring(0, word.length() - 3);

        EnId minone = enIdRepository.findByWord(word.substring(0, word.length() - 1));
        EnId mintwo = enIdRepository.findByWord(word.substring(0, word.length() - 2));
        EnId minthree = enIdRepository.findByWord(cutLastThree);
        EnId ied = enIdRepository.findByWord(cutLastThree + "y");
        EnId plusFes = enIdRepository.findByWord(cutLastThree + "fe");
        EnId plusF = enIdRepository.findByWord(cutLastThree + "f");
        EnId plusSis = enIdRepository.findByWord(cutLastThree + "sis");

        EnId[] possiblities = {minone, mintwo, ied, plusFes, plusF, plusSis, minthree};

        for (EnId poss : possiblities) {
            if (poss != null) {
                return new String[]{"", poss.getTrans(),
                    ", plural form of " + "\"" + poss.getWord() + "\""};
            }
        }
        return new String[]{"", "Kata tidak dapat ditemukan di kamus", ""};
    }

    public String toString() {
        return "CheckPluralParser";
    }
}
