package com.doku.translate.model;

import org.springframework.stereotype.Component;

@Component
public class ParseStrategy {
    private Parser parser;

    public void setParser(Parser parser) {
        this.parser = parser;
    }

    public Parser getParser() {
        return parser;
    }

    public String[] parse(String word) {
        return parser.parse(word);
    }

}
