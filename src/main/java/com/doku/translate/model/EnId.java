package com.doku.translate.model;

import com.doku.level.model.UserWord;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "data_en_id")
public class EnId {
    @Id
    @Column(name = "word",
            nullable = false,
            columnDefinition = "TEXT")
    private String word;

    @Column(name = "trans",
            nullable = false,
            columnDefinition = "TEXT")
    private String trans;

    @OneToMany(mappedBy = "word", cascade = CascadeType.MERGE)
    private Set<UserWord> users = new HashSet<>();

    @Column(name = "note")
    private String note;

    public EnId(String word, String trans) {
        this.word = word;
        this.trans = trans;
    }

    /**
     * constructor for a word object.
     * @summary constructor for a word object
     * @param word the word
     * @param trans the Indonesian translation of the word
     * @param note note on what form the word is
     */
    public EnId(String word, String trans, String note) {
        this.word = word;
        this.trans = trans;
        this.note = note;
    }

    public EnId() {

    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTrans() {
        return trans;
    }

    public String getNote() {
        return this.note;
    }

    public Boolean checkUser(UserWord userWord) {
        return users.contains(userWord);
    }

    public void addUser(UserWord userword) {
        users.add(userword);
    }

    public void replaceWord(UserWord old, UserWord nieuw) {
        users.remove(old);
        users.add(nieuw);
    }

    public String toString() {
        return this.word + " : " + this.trans;
    }
}
