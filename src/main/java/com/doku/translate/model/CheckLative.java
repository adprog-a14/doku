package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckLative implements Parser {
    @Autowired
    EnIdRepository enIdRepository;

    @Override
    public String[] parse(String word) {
        String note = whatLative(word);
        String cutLastThree = word.substring(0, word.length() - 3);

        EnId minone = enIdRepository.findByWord(word.substring(0, word.length() - 1));
        EnId mintwo = enIdRepository.findByWord(word.substring(0, word.length() - 2));
        EnId minthree = enIdRepository.findByWord(cutLastThree);
        EnId plusIer = enIdRepository.findByWord(cutLastThree + "y");
        EnId plusIest = enIdRepository.findByWord(word.substring(0, word.length() - 4) + "y");

        EnId[] possiblities = {minone, mintwo, minthree, plusIer, plusIest};

        for (EnId poss : possiblities) {
            if (poss != null) {
                return new String[]{"", poss.getTrans(), note + "\"" + poss.getWord() + "\""};
            }
        }
        return new String[]{"", "Kata tidak dapat ditemukan di kamus", ""};
    }

    public String toString() {
        return "CheckLativeParser";
    }

    private String whatLative(String word) {
        if (word.endsWith("er")) {
            return ", comparative form of ";
        }
        return ", superlative form of ";
    }
}
