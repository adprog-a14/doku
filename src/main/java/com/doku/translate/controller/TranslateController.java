package com.doku.translate.controller;

import com.doku.translate.service.EnIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class TranslateController {
    @Autowired
    private EnIdService enIdService;

    @PostMapping(path = "/translate", produces = { "application/json" })
    public ResponseEntity translate(@RequestBody String word) {
        return ResponseEntity.ok(enIdService.getTransbyWord(word));
    }
}
