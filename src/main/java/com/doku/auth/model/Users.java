package com.doku.auth.model;

import com.doku.level.model.UserWord;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USERS")
@Data
public class Users {

    @Id
    private String username;
    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
    private Set<UserWord> words = new HashSet<>();

    public Users() {
    }

    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void addWord(UserWord userword) {
        words.add(userword);
    }

    public void replaceWord(UserWord old, UserWord nieuw) {
        words.remove(old);
        words.add(nieuw);
    }

    public String toString() {
        return username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

//package com.doku.Auth.model;
//
//        import com.doku.Level.model.UserWord;
//        import lombok.Data;
//        import lombok.NoArgsConstructor;
//        import org.hibernate.annotations.WhereJoinTable;
//        import org.springframework.security.core.GrantedAuthority;
//        import org.springframework.security.core.userdetails.User;
//        import org.springframework.security.core.userdetails.UserDetails;
//
//        import javax.persistence.*;
//        import java.util.Collection;
//        import java.util.HashSet;
//        import java.util.Set;

//@Entity
//@Table(name = "USERS")
//@Data
//@NoArgsConstructor
//public class Account {
//
//    @Id
//    private String username;
//    private String password;
//
//    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
//    private Set<UserWord> words = new HashSet<>();
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public void setWords(Set<UserWord> words) {
//        this.words = words;
//    }
//
//    public void addWord(UserWord userword) {
//        words.add(userword);
//    }
//
//    public void replaceWord(UserWord old, UserWord nieuw) {
//        words.remove(old);
//        words.add(nieuw);
//    }
//
//    public String toString() {
//        return username;
//    }
//}

