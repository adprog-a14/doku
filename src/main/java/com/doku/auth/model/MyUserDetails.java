package com.doku.auth.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


public class MyUserDetails implements UserDetails {

    private String username;
    private String password;
    private List<GrantedAuthority> authorities;

    /**
     * Konstruktor untuk membuat MyUserDertails.
     * @param users akun yang akan digunakan
     */
    public MyUserDetails(Users users) {
        this.username = users.getUsername();
        this.password = users.getPassword();

        List<GrantedAuthority> userAuthority = new ArrayList<>();
        userAuthority.add(new SimpleGrantedAuthority("USER"));
        this.authorities =  userAuthority;
    }


    /**
     * Getter authority user.
     * @return mengembalikan role yang dimiliki user
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * Getter password user.
     * @return password
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * Getter username user.
     * @return username
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * Default dijadikan true.
     * @return true
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Default dijadikan true.
     * @return true
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Default dijadikan true.
     * @return true
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Default dijadikan true.
     * @return true
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
