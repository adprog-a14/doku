package com.doku.extractpdf.service;

import java.io.IOException;
import java.util.ArrayList;
import org.springframework.web.multipart.MultipartFile;

public interface ExtractPdfService {
    ArrayList<ArrayList<String[]>> getText(MultipartFile file) throws IOException;
}
