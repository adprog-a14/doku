package com.doku.extractpdf.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ExtractPdfServiceImpl implements ExtractPdfService {
    private PDFParser parser;
    private RandomAccessFile raf;
    private PDFTextStripper pdfStripper;
    private PDDocument pdDoc;
    private COSDocument cosDoc;

    private ArrayList<ArrayList<String[]>> text = new ArrayList<>();
    private File file;

    public ExtractPdfServiceImpl() {

    }

    /**
     * get text from PDF.
     * @summary get text from PDF
     * @param multipart multipart file PDF
     * @return arraylist of arraylist of string containing PDF words
     * @throws IOException if something's wrong with the file
     */
    public ArrayList<ArrayList<String[]>> getText(MultipartFile multipart) throws IOException {
        resetAttributes();
        prepareFile(multipart);
        extractText();
        closeAndDelete();

        return text;
    }

    //Nullify and clear attributes necessary
    // for extracting texts from PDF so that it can be used for the next PDF
    private void resetAttributes() {
        file = null;
        parser = null;
        raf = null;
        pdfStripper = null;
        pdDoc = null;
        cosDoc = null;
        text.clear();
    }

    //Prepare PDF file submitted by user to be extracted
    private void prepareFile(MultipartFile multipart) throws IOException {
        this.file = File.createTempFile("prefix", "suffix");
        multipart.transferTo(file);
        raf = new RandomAccessFile(this.file, "r");
        parser = new PDFParser(raf);
        parser.parse();
        cosDoc = parser.getDocument();
        pdfStripper = new PDFTextStripper();
        pdDoc = new PDDocument(cosDoc);
    }

    //Extract the texts from PDF using PDFBOX
    private void extractText() throws IOException {
        for (int i = 0; i <= pdDoc.getNumberOfPages(); i++) {
            boolean whiteSpaceFlag = false;
            ArrayList<String[]> page = new ArrayList<>();

            pdfStripper.setStartPage(i);
            pdfStripper.setEndPage(i);
            pdfStripper.setParagraphStart("/t");
            pdfStripper.setSortByPosition(true);

            for (String line: pdfStripper.getText(pdDoc).split(pdfStripper.getParagraphStart())) {
                String[] splittedLine = line.split(" ");

                if (!(splittedLine.length == 1 && splittedLine[0].equals("")) && !whiteSpaceFlag) {
                    whiteSpaceFlag = true;
                }

                if (whiteSpaceFlag) {
                    page.add(line.split(" "));
                }
            }

            if (page.size() != 0) {
                if (!(page.size() == 1 && page.get(0)[0].equals(""))) {
                    text.add(page);
                }
            }
        }
    }

    //Close attributes and delete the PDF file after used
    private void closeAndDelete() throws IOException {
        cosDoc.close();
        pdDoc.close();
        raf.close();
        file.setWritable(true);
        file.delete();
    }
}
