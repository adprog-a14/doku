package com.doku.extractpdf.controller;

import com.doku.extractpdf.service.ExtractPdfService;
import java.io.IOException;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ExtractPdfController {
    @Autowired
    private ExtractPdfService extractPdfService;

    private ArrayList<ArrayList<String[]>> words;

    @GetMapping("/")
    public String getUploadPdf() {
        return "ExtractPDF/extractPDF";
    }

    /**
     * postmapping to reading mode.
     * @summary postmapping to reading mode
     * @param model the model
     * @param file the PDF file uploaded by user
     * @return redirect to "/"
     * @throws IOException if something's wrong with the file
     */
    @PostMapping("/ReadingMode")
    public String upload(Model model, @RequestParam("file") MultipartFile file) throws IOException {

        if (file.getContentType().equalsIgnoreCase("application/pdf")) {
            words = extractPdfService.getText(file);
            model.addAttribute("words", words.get(0));
            return "ExtractPDF/testsuccess";
        }
        return "redirect:/";

    }

    @GetMapping(path = "/ReadingMode", produces = { "application/json" })
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity getPdfText() {
        return ResponseEntity.ok(words);
    }
}
