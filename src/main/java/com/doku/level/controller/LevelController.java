package com.doku.level.controller;

import com.doku.level.service.LevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class LevelController {
    @Autowired
    LevelService levelService;

    @PostMapping(path = "/assignLevel/{level}", produces = { "application/json" })
    @ResponseStatus(value = HttpStatus.OK)
    public void assignLevel(@RequestBody String word,
                    @PathVariable(value = "level") Integer level) {
        levelService.assignLevel(word, level);
    }

    @GetMapping(path = "/getUserLevel", produces = { "application/json" })
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity getUserLevel() {
        return ResponseEntity.ok(levelService.getUserLevel());
    }
}
