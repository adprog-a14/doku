package com.doku.level.service;

import java.util.Map;

public interface LevelService {
    void assignLevel(String word, Integer level);

    Map<String, Integer> getUserLevel();
}
