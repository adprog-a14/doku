package com.doku.level.service;

import com.doku.auth.UserRepository;
import com.doku.auth.model.Users;
import com.doku.level.model.UserWord;
import com.doku.level.repository.UserWordRepository;
import com.doku.translate.model.EnId;
import com.doku.translate.repository.EnIdRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class LevelServiceImpl implements LevelService {
    @Autowired
    EnIdRepository enidRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserWordRepository userWordRepository;

    //Assign level of understanding to a word for the user
    @Override
    public void assignLevel(String word, Integer level) {
        word = word.replaceAll("[^a-zA-Z]", "").toLowerCase();
        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication().getName()).get();
        EnId enWord = enidRepository.findByWord(word);
        UserWord newUW = new UserWord(user, enWord, level);

        saveWord(user, enWord, newUW);
    }

    /**
     * method to get user level on a word.
     * @summary method to get user level on a word
     * @return the user's already learned words and their levels in a hashmap
     */
    public Map<String, Integer> getUserLevel() {
        List<EnId>[] words = getUserWords();
        HashMap<String, Integer> wordLevels = new HashMap<>();

        for (int i = 0; i < words.length; i++) {
            for (EnId word : words[i]) {
                wordLevels.put(word.getWord(), i + 1);
            }
        }

        return wordLevels;
    }

    //Save the word to user's learned words repository
    private void saveWord(Users user, EnId enWord, UserWord newUW) {
        UserWord saved = user.getWords().stream()
                .filter(e -> e.getWord().equals(enWord)).findFirst().orElse(null);

        if (saved == null) {
            enWord.addUser(newUW);
            user.addWord(newUW);
        } else {
            enWord.replaceWord(saved, newUW);
            user.replaceWord(saved, newUW);
            userWordRepository.delete(saved);
        }

        userWordRepository.save(newUW);
    }


    //Take user's learned words as a list to be converted to hashmaps later in getUserLevel()
    private List<EnId>[] getUserWords() {
        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication().getName()).get();

        List<EnId> levelOne = userWordRepository.findWordbyLevel(user, 1);
        List<EnId> levelTwo = userWordRepository.findWordbyLevel(user, 2);
        List<EnId> levelThree = userWordRepository.findWordbyLevel(user, 3);
        List<EnId> levelFour = userWordRepository.findWordbyLevel(user, 4);
        List<EnId> levelFive = userWordRepository.findWordbyLevel(user, 5);

        return new List[] {levelOne, levelTwo, levelThree, levelFour, levelFive};
    }
}
