package com.doku.level.repository;

import com.doku.auth.model.Users;
import com.doku.level.model.CompositeKey;
import com.doku.level.model.UserWord;
import com.doku.translate.model.EnId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserWordRepository extends JpaRepository<UserWord, CompositeKey> {
    @Query("select uw.word from user_word uw where uw.user = ?1 and uw.level = ?2")
    List<EnId> findWordbyLevel(Users user, Integer level);
}
