package com.doku.level.model;

import java.io.Serializable;

public class CompositeKey implements Serializable {
    private String user;
    private String word;
}
