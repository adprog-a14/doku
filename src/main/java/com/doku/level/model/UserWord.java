package com.doku.level.model;

import com.doku.auth.model.Users;
import com.doku.translate.model.EnId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "user_word")
@Table(name = "user_word")
@IdClass(CompositeKey.class)
public class UserWord {

    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "username", insertable = false, updatable = false)
    private Users user;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "word", insertable = false, updatable = false)
    private EnId word;

    @Column
    private Integer level;


    /**
     * userword constructor.
     * @summary userword constructor
     * @param user the user object
     * @param word the word that user learned
     * @param level the level of the word
     */
    public UserWord(Users user, EnId word, Integer level) {
        this.user = user;
        this.word = word;
        this.level = level;
    }

    public UserWord() {

    }

    public void setWord(EnId word) {
        this.word = word;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public EnId getWord() {
        return word;
    }

    public Integer getLevel() {
        return level;
    }

    public String toString() {
        return user + " " + word + " " + level;
    }

}
