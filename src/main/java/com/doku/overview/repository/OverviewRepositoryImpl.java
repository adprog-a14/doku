package com.doku.overview.repository;

import com.doku.auth.model.Users;
import com.doku.translate.model.EnId;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class OverviewRepositoryImpl implements OverviewRepository {

    private Map<String, Integer> mapJmlKataBar = new LinkedHashMap<>();
    private Map<String, Integer> mapJmlKataPie = new LinkedHashMap<>();
    private ArrayList<List<EnId>> wordLevel = new ArrayList<>();
    private Map<Users, Integer[]> mapBarUser = new LinkedHashMap<>();
    private Map<Users, Integer[]> mapPieUser = new LinkedHashMap<>();

    @Override
    public void saveJmlKataBar(String level, Integer jumlah) {
        mapJmlKataBar.put(level, jumlah);
    }

    @Override
    public void saveJmlKataPie(String level, Integer jumlah) {
        mapJmlKataPie.put(level, jumlah);
    }

    @Override
    public void saveWord(List<EnId> words) {
        wordLevel.add(words);
    }

    @Override
    public Integer[] saveListJumlahBar() {
        Integer[] tempList = new Integer[5];
        tempList[0] = mapJmlKataBar.remove("1");
        tempList[1] = mapJmlKataBar.remove("2");
        tempList[2] = mapJmlKataBar.remove("3");
        tempList[3] = mapJmlKataBar.remove("4");
        tempList[4] = mapJmlKataBar.remove("5");
        return tempList;
    }

    @Override
    public Integer[] saveListJumlahPie() {
        Integer[] tempList = new Integer[5];
        tempList[0] = mapJmlKataPie.remove("1");
        tempList[1] = mapJmlKataPie.remove("2");
        tempList[2] = mapJmlKataPie.remove("3");
        tempList[3] = mapJmlKataPie.remove("4");
        tempList[4] = mapJmlKataPie.remove("5");
        return tempList;
    }

    @Override
    public void saveUserAndAmountBar(Users users) {
        mapBarUser.put(users, saveListJumlahBar());
    }

    @Override
    public void saveUserAndAmountPie(Users users) {
        mapPieUser.put(users, saveListJumlahPie());
    }

    @Override
    public Map<String, Integer> getMapBar(Users users) {
        if (mapBarUser.containsKey(users)) {
            Integer[] jmlKata = mapBarUser.get(users);
            Map<String, Integer> mapForView = new LinkedHashMap<>();
            mapForView.put("1", jmlKata[0]);
            mapForView.put("2", jmlKata[1]);
            mapForView.put("3", jmlKata[2]);
            mapForView.put("4", jmlKata[3]);
            mapForView.put("5", jmlKata[4]);
            return mapForView;
        }
        return null;
    }

    @Override
    public Map<String, Integer> getMapPie(Users users) {
        if (mapPieUser.containsKey(users)) {
            Integer[] jmlKata = mapPieUser.get(users);
            Map<String, Integer> mapForView = new LinkedHashMap<>();
            mapForView.put("1", jmlKata[0]);
            mapForView.put("2", jmlKata[1]);
            mapForView.put("3", jmlKata[2]);
            mapForView.put("4", jmlKata[3]);
            mapForView.put("5", jmlKata[4]);
            wordLevel.clear();
            return mapForView;
        }
        return null;
    }

    @Override
    public ArrayList<List<EnId>> getListWord() {
        if (wordLevel.size() == 0) {
            return null;
        }
        return wordLevel;
    }

    @Override
    public Users checkUserBar(Users users) {
        if (mapBarUser.containsKey(users)) {
            return users;
        }
        return null;
    }

    @Override
    public Users checkUserPie(Users users) {
        if (mapPieUser.containsKey(users)) {
            return users;
        }
        return null;
    }
}