package com.doku.overview.repository;

import com.doku.auth.model.Users;
import com.doku.translate.model.EnId;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface OverviewRepository {

    void saveJmlKataBar(String level, Integer jumlah);

    void saveJmlKataPie(String level, Integer jumlah);

    void saveWord(List<EnId> userWords);

    void saveUserAndAmountBar(Users user);

    void saveUserAndAmountPie(Users user);

    Integer[] saveListJumlahBar();

    Integer[] saveListJumlahPie();

    Map<String, Integer> getMapBar(Users users);

    Map<String, Integer> getMapPie(Users users);

    ArrayList<List<EnId>> getListWord();

    Users checkUserBar(Users users);

    Users checkUserPie(Users users);
}
