package com.doku.overview.controller;

import com.doku.overview.service.OverviewService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OverviewController {

    @Autowired
    private OverviewService overviewService;

    /**
     * @GetMapping specifies that when an
     *     HTTP GET request is received for /overview,
     *     getDiagram() will be called to handle the request..
     *     in getDiagram() method, there are many attributes that will be parsed into HTML
     *     barChartData has data to show in bar chart
     *     jumlahKata has integer value to scale the bar chart
     *     mapPie has data to show in pie chart
     * @param model is some model
     */

    @GetMapping("/overview")
    public String getDiagram(Model model) {
        model.addAttribute("barChartData", overviewService.getMapBar());
        model.addAttribute("jumlahKata", overviewService.lengthWordList());
        Map<String, Integer> mapPie = overviewService.getMapPie();
        model.addAttribute("pieSatu", mapPie.get("1"));
        model.addAttribute("pieDua", mapPie.get("2"));
        model.addAttribute("pieTiga", mapPie.get("3"));
        model.addAttribute("pieEmpat", mapPie.get("4"));
        model.addAttribute("pieLima", mapPie.get("5"));
        return "overview/overviews";
    }
}
