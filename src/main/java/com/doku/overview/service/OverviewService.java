package com.doku.overview.service;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface OverviewService {
    Map<String, Integer> getMapBar();

    Map<String, Integer> getMapPie();

    void saveMapBar();

    void saveMapPie();

    void saveNullListKata(String chart);

    int lengthWordList();

}
