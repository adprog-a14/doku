package com.doku.overview.service;

import com.doku.auth.UserRepository;
import com.doku.auth.model.Users;
import com.doku.level.repository.UserWordRepository;
import com.doku.overview.repository.OverviewRepository;
import com.doku.translate.model.EnId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class OverviewServiceImpl implements OverviewService {

    @Autowired
    private OverviewRepository overviewRepository;

    @Autowired
    private UserWordRepository userWordRepository;

    @Autowired
    private UserRepository userRepository;

    private int total;

    @Override
    public Map<String, Integer> getMapBar() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userRepository.findUserByUsername(username).get();
        if (overviewRepository.checkUserBar(user) == null) {
            overviewRepository.saveWord(userWordRepository.findWordbyLevel(user,1));
            overviewRepository.saveWord(userWordRepository.findWordbyLevel(user,2));
            overviewRepository.saveWord(userWordRepository.findWordbyLevel(user,3));
            overviewRepository.saveWord(userWordRepository.findWordbyLevel(user,4));
            overviewRepository.saveWord(userWordRepository.findWordbyLevel(user,5));
            saveMapBar();
            overviewRepository.saveUserAndAmountBar(user);
        }
        return overviewRepository.getMapBar(user);
    }

    @Override
    public Map<String, Integer> getMapPie() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = userRepository.findUserByUsername(username).get();
        if (overviewRepository.checkUserPie(user) == null) {
            saveMapPie();
            overviewRepository.saveUserAndAmountPie(user);
        }
        return overviewRepository.getMapPie(user);
    }

    @Override
    public void saveMapBar() {
        ArrayList<List<EnId>> wordLevel = overviewRepository.getListWord();
        if (wordLevel == null) {
            saveNullListKata("bar");
        }
        if (wordLevel.get(0) == null) {
            overviewRepository.saveJmlKataBar("1", 0);
        }
        if (wordLevel.get(1) == null) {
            overviewRepository.saveJmlKataBar("2", 0);
        }
        if (wordLevel.get(2) == null) {
            overviewRepository.saveJmlKataBar("3", 0);
        }
        if (wordLevel.get(3) == null) {
            overviewRepository.saveJmlKataBar("4", 0);
        }
        if (wordLevel.get(4) == null) {
            overviewRepository.saveJmlKataBar("5", 0);
        }
        overviewRepository.saveJmlKataBar("1", wordLevel.get(0).size());
        overviewRepository.saveJmlKataBar("2", wordLevel.get(1).size());
        overviewRepository.saveJmlKataBar("3", wordLevel.get(2).size());
        overviewRepository.saveJmlKataBar("4", wordLevel.get(3).size());
        overviewRepository.saveJmlKataBar("5", wordLevel.get(4).size());
    }

    @Override
    public void saveMapPie() {
        ArrayList<List<EnId>> wordLevel = overviewRepository.getListWord();
        if (wordLevel == null) {
            saveNullListKata("pie");
        } else {
            int sizeSatu = wordLevel.get(0).size();
            int sizeDua = wordLevel.get(1).size();
            int sizeTiga = wordLevel.get(2).size();
            int sizeEmpat = wordLevel.get(3).size();
            int sizeLima = wordLevel.get(4).size();
            total = sizeSatu + sizeDua + sizeTiga + sizeEmpat + sizeLima;
            if (total == 0) {
                saveNullListKata("pie");
            } else {
                overviewRepository.saveJmlKataPie("1", countPersen(wordLevel.get(0).size(), total));
                overviewRepository.saveJmlKataPie("2", countPersen(wordLevel.get(1).size(), total));
                overviewRepository.saveJmlKataPie("3", countPersen(wordLevel.get(2).size(), total));
                overviewRepository.saveJmlKataPie("4", countPersen(wordLevel.get(3).size(), total));
                overviewRepository.saveJmlKataPie("5", countPersen(wordLevel.get(4).size(), total));
            }
        }
    }

    @Override
    public void saveNullListKata(String chart) {
        if (chart.equals("bar")) {
            overviewRepository.saveJmlKataBar("1", 0);
            overviewRepository.saveJmlKataBar("2", 0);
            overviewRepository.saveJmlKataBar("3", 0);
            overviewRepository.saveJmlKataBar("4", 0);
            overviewRepository.saveJmlKataBar("5", 0);
        } else {
            overviewRepository.saveJmlKataPie("1", 0);
            overviewRepository.saveJmlKataPie("2", 0);
            overviewRepository.saveJmlKataPie("3", 0);
            overviewRepository.saveJmlKataPie("4", 0);
            overviewRepository.saveJmlKataPie("5", 0);
        }
    }

    @Override
    public int lengthWordList() {
        int skalaDiagram = total + 15;
        return skalaDiagram;
    }

    private int countPersen(int nilai, int total) {
        int perkalian = nilai * 100;
        double persen = (perkalian / total);
        int hasil = (int) persen;
        return hasil;
    }
}