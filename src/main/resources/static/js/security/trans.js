let words = document.getElementsByTagName('span');
let mean = document.getElementById('meaning');

for(var i = 0; i < words.length; i++){
  if (words[i].innerText !== '') {
    let word = words[i].innerText
    words[i].onclick = function() {
      translateK(word)
    }
  }
}

function translateK(word){
  var req = new XMLHttpRequest()
  req.open('POST', '/translate')
  req.onload = function() {
    mean.innerText = req.responseText
    console.log(req.responseText)
  }
  req.send(word)
}