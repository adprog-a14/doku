package com.doku.extractpdf.controller;

import com.doku.auth.UserRepository;
import com.doku.extractpdf.service.ExtractPdfService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.InputStream;
import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ExtractPdfController.class)
public class ExtractPdfControllerTest {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private ExtractPdfService extractPDFService;

    @Test
    @WithMockUser
    public void getUploadPDFShouldBeOKAndReturnExtractPDFView() throws Exception{
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("ExtractPDF/extractPDF"));
    }

    @Test
    @WithMockUser
    public void postReadingModeShouldBeOkAndReturnSuccess() throws Exception {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("test.pdf");
        MockMultipartFile file = new MockMultipartFile("file", "test.pdf", "application/pdf", inputStream);

        ArrayList<ArrayList<String[]>> mocked = new ArrayList<>();
        mocked.add(new ArrayList<>());
        mocked.get(0).add(new String[] {"a"});

        when(extractPDFService.getText(file)).thenReturn(mocked);

        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(multipart("/ReadingMode").file(file)).andExpect(status().isOk())
                .andExpect(view().name("ExtractPDF/testsuccess"));

        verify(extractPDFService, times(1)).getText(file);
    }

    @Test
    @WithMockUser
    public void postReadingModeShouldRedirectIfNonPDFUploaded() throws Exception {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("memri.jpg");
        MockMultipartFile file = new MockMultipartFile("file", "memri.jpg", "image/jpg", inputStream);

        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(multipart("/ReadingMode").file(file)).andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));

        verify(extractPDFService, times(0)).getText(file);
    }

    @Test
    @WithMockUser
    void testGetPdfText() throws Exception {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("test.pdf");
        MockMultipartFile file = new MockMultipartFile("file", "test.pdf", "application/pdf", inputStream);

        ArrayList<ArrayList<String[]>> mocked = new ArrayList<>();
        mocked.add(new ArrayList<>());
        mocked.get(0).add(new String[] {"a"});

        when(extractPDFService.getText(file)).thenReturn(mocked);

        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(multipart("/ReadingMode").file(file));

        mockMvc.perform(get("/ReadingMode"))
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}
