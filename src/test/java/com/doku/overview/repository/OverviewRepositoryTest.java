package com.doku.overview.repository;

import com.doku.auth.UserRepository;
import com.doku.auth.model.Users;
import com.doku.level.repository.UserWordRepository;
import com.doku.translate.model.EnId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
@ExtendWith(MockitoExtension.class)
public class OverviewRepositoryTest {

    @Autowired
    private OverviewRepository overviewRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserWordRepository userWordRepository;

    @Mock
    private Map<String, Integer> mapJmlKataBar;

    @Mock
    private Map<String, Integer> mapJmlKataPie;

    @Mock
    private ArrayList<List<EnId>> wordLevel;

    @Mock
    private Map<Users, Integer[]> mapBarUser;

    @Mock
    private Map<Users, Integer[]> mapPieUser;

    @BeforeEach
    @WithMockUser
    public void setUp() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(new Users()));
        SecurityContextHolder.setContext(securityContext);

        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        overviewRepository = new OverviewRepositoryImpl();
        mapJmlKataBar = new LinkedHashMap<>();
        mapJmlKataPie = new LinkedHashMap<>();
        wordLevel = new ArrayList<>();
        mapBarUser = new LinkedHashMap<>();
        mapPieUser = new LinkedHashMap<>();
        overviewRepository.saveUserAndAmountBar(user);
        overviewRepository.saveUserAndAmountPie(user);
    }

    @Test
    public void OverviewRepoSaveJmlKataBarAndPieItShouldSaveLevelAndAmount() {
        ReflectionTestUtils.setField(overviewRepository, "mapJmlKataBar", mapJmlKataBar);
        ReflectionTestUtils.setField(overviewRepository, "mapJmlKataPie", mapJmlKataPie);
        overviewRepository.saveJmlKataBar("2", 20);
        overviewRepository.saveJmlKataPie("2", 20);
        int valueBar = mapJmlKataBar.get("2");
        int valuePie = mapJmlKataPie.get("2");
        assertThat(valueBar).isEqualTo(20);
        assertThat(valuePie).isEqualTo(20);
    }

    @Test
    public void OverviewRepoSaveWordShouldSaveListOfWord() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(new Users()));
        SecurityContextHolder.setContext(securityContext);

        ReflectionTestUtils.setField(overviewRepository, "wordLevel", wordLevel);
        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        overviewRepository.saveWord(userWordRepository.findWordbyLevel(user, 1));
        List<EnId> wordLevelList = wordLevel.get(0);
        String stringWords = "";
        for(EnId words : wordLevelList) {
            stringWords += words + ",";
        }

        assertThat(stringWords).isEqualTo("");
    }

    @Test
    public void OverviewRepoSaveListJumlahShouldSaveJumlahKataBarAndPieIntoList() {
        ReflectionTestUtils.setField(overviewRepository, "mapJmlKataBar", mapJmlKataBar);
        ReflectionTestUtils.setField(overviewRepository, "mapJmlKataPie", mapJmlKataPie);
        mapJmlKataBar.put("1", 10);
        mapJmlKataBar.put("2", 0);
        mapJmlKataBar.put("3", 0);
        mapJmlKataBar.put("4", 0);
        mapJmlKataBar.put("5", 0);
        mapJmlKataPie.put("1", 10);
        mapJmlKataPie.put("2", 0);
        mapJmlKataPie.put("3", 0);
        mapJmlKataPie.put("4", 0);
        mapJmlKataPie.put("5", 0);
        Integer[] tempList = new Integer[5];
        tempList[0] = 10;
        tempList[1] = 0;
        tempList[2] = 0;
        tempList[3] = 0;
        tempList[4] = 0;
        Integer[] testMethodSaveListBar = overviewRepository.saveListJumlahBar();
        Integer[] testMethodSaveListPie = overviewRepository.saveListJumlahPie();

        assertThat(testMethodSaveListBar).isEqualTo(tempList);
        assertThat(testMethodSaveListPie).isEqualTo(tempList);
    }

    @Test
    public void OverviewRepoSaveUserAndAmountShouldSaveIt() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(new Users()));
        SecurityContextHolder.setContext(securityContext);

        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        ReflectionTestUtils.setField(overviewRepository, "mapBarUser", mapBarUser);
        ReflectionTestUtils.setField(overviewRepository, "mapPieUser", mapPieUser);
        overviewRepository.saveUserAndAmountBar(user);
        overviewRepository.saveUserAndAmountPie(user);
        boolean checkUserBar = mapBarUser.containsKey(user);
        boolean checkUserPie = mapPieUser.containsKey(user);

        assertThat(checkUserBar).isEqualTo(true);
        assertThat(checkUserPie).isEqualTo(true);
    }

    @Test
    public void OverviewRepoGetMapBarAndPieItShouldReturnHashMapChart() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(new Users()));
        SecurityContextHolder.setContext(securityContext);

        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        ReflectionTestUtils.setField(overviewRepository, "mapBarUser", mapBarUser);
        ReflectionTestUtils.setField(overviewRepository, "mapPieUser", mapPieUser);
        Map<String, Integer> mapBarChartFail = overviewRepository.getMapBar(user);
        Map<String, Integer> mapPieChartFail = overviewRepository.getMapPie(user);
        if (!mapBarUser.containsKey(user) || !mapPieUser.containsKey(user)) {
            assertThat(mapBarChartFail).isEqualTo(null);
            assertThat(mapPieChartFail).isEqualTo(null);
        }

        overviewRepository.saveUserAndAmountBar(user);
        overviewRepository.saveUserAndAmountPie(user);
        Map<String, Integer> mapBarChart = overviewRepository.getMapBar(user);
        Map<String, Integer> mapPieChart = overviewRepository.getMapPie(user);
        Integer[] jmlKataBar = mapBarUser.get(user);
        Integer[] jmlKataPie = mapPieUser.get(user);
        Map<String, Integer> mapBarForView = new LinkedHashMap<>();
        mapBarForView.put("1", jmlKataBar[0]);
        mapBarForView.put("2", jmlKataBar[1]);
        mapBarForView.put("3", jmlKataBar[2]);
        mapBarForView.put("4", jmlKataBar[3]);
        mapBarForView.put("5", jmlKataBar[4]);
        Map<String, Integer> mapPieForView = new LinkedHashMap<>();
        mapPieForView.put("1", jmlKataPie[0]);
        mapPieForView.put("2", jmlKataPie[1]);
        mapPieForView.put("3", jmlKataPie[2]);
        mapPieForView.put("4", jmlKataPie[3]);
        mapPieForView.put("5", jmlKataPie[4]);

        assertThat(mapBarChart).isEqualTo(mapBarForView);
        assertThat(mapPieChart).isEqualTo(mapPieForView);
    }

    @Test
    public void OverviewRepoGetListWordShouldReturnArrayList() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(new Users()));
        SecurityContextHolder.setContext(securityContext);

        ReflectionTestUtils.setField(overviewRepository, "wordLevel", wordLevel);
        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();

        ArrayList<List<EnId>> wordLevelListFail = overviewRepository.getListWord();
        assertThat(wordLevelListFail).isEqualTo(null);
        overviewRepository.saveWord(userWordRepository.findWordbyLevel(user, 1));

        ArrayList<List<EnId>> wordLevelList = overviewRepository.getListWord();
        String stringWords = "";
        for(List<EnId> listWord : wordLevelList) {
            for (EnId words : listWord) {
                stringWords = stringWords + words + ",";
            }
        }

        assertThat(stringWords).isEqualTo("");
    }

    @Test
    public void OverviewRepoCheckUserShouldReturnUser() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(new Users()));
        SecurityContextHolder.setContext(securityContext);

        ReflectionTestUtils.setField(overviewRepository, "mapBarUser", mapBarUser);
        ReflectionTestUtils.setField(overviewRepository, "mapPieUser", mapPieUser);
        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        Users barUserFail = overviewRepository.checkUserBar(user);
        Users pieUserFail = overviewRepository.checkUserPie(user);
        if (!mapBarUser.containsKey(user) || !mapPieUser.containsKey(user)) {
            assertThat(barUserFail).isEqualTo(null);
            assertThat(pieUserFail).isEqualTo(null);
        }

        overviewRepository.saveUserAndAmountBar(user);
        overviewRepository.saveUserAndAmountPie(user);
        Users barUser = overviewRepository.checkUserBar(user);
        Users pieUser = overviewRepository.checkUserPie(user);
        assertThat(barUser).isEqualTo(user);
        assertThat(pieUser).isEqualTo(user);
    }
}