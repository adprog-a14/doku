package com.doku.overview.service;

import com.doku.auth.UserRepository;
import com.doku.auth.model.Users;
import com.doku.level.model.UserWord;
import com.doku.level.repository.UserWordRepository;
import com.doku.level.service.LevelService;
import com.doku.overview.repository.OverviewRepositoryImpl;
import com.doku.translate.model.EnId;
import com.doku.overview.repository.OverviewRepository;
import com.doku.translate.repository.EnIdRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OverviewServiceTest {

    @Mock
    private OverviewRepository overviewRepository;

    @Mock
    private OverviewRepository overviewRepo;

    @Mock
    private UserWordRepository userWordRepository;

    @Mock
    private EnIdRepository enIdRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private LevelService levelService;

    @InjectMocks
    private OverviewService overviewServiceImpl = new OverviewServiceImpl();

    @Mock
    private ArrayList<List<EnId>> wordLevel;

    private String[] abominablyExpected = new String[]{"", "ks. buruk sekali (of food, style).", ", adverb form of \"abominable\""};
    private String[] transmuteExpected = new String[]{"", "kkt. mengubah."};
    private String[] townsfolkExpected = new String[]{"", "kb. orang-orang kota."};
    private String[] thereforeExpected = new String[]{"", "kk. oleh karena itu."};
    private String[] eyeglassesExpected = new String[]{"", "kb. j. kacamata, tesmak."};

    @BeforeEach
    @WithMockUser
    public void setUp() {
        Users user = new Users();
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        lenient().when(securityContext.getAuthentication()).thenReturn(authentication);
        lenient().when(authentication.getName()).thenReturn("user1");
        lenient().when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(user));
        SecurityContextHolder.setContext(securityContext);

        EnId mockedWord5 = new EnId("abominable", abominablyExpected[1], abominablyExpected[2]);
        user.addWord(new UserWord(user, mockedWord5, 5));
        EnId mockedWord4 = new EnId("transmute", transmuteExpected[1]);
        user.addWord(new UserWord(user, mockedWord4, 4));
        EnId mockedWord3 = new EnId("townsfolk", townsfolkExpected[1]);
        user.addWord(new UserWord(user, mockedWord3, 3));
        EnId mockedWord2 = new EnId("therefore", thereforeExpected[1]);
        user.addWord(new UserWord(user, mockedWord2, 2));
        EnId mockedWord1 = new EnId("eyeglasses", eyeglassesExpected[1]);
        user.addWord(new UserWord(user, mockedWord1, 1));
        overviewRepository = new OverviewRepositoryImpl();
        List<EnId> word5 = new ArrayList<>();
        word5.add(mockedWord5);
        List<EnId> word4 = new ArrayList<>();
        word4.add(mockedWord4);
        List<EnId> word3 = new ArrayList<>();
        word3.add(mockedWord3);
        List<EnId> word2 = new ArrayList<>();
        word2.add(mockedWord2);
        List<EnId> word1 = new ArrayList<>();
        word1.add(mockedWord1);
        overviewRepository.saveWord(word1);
        overviewRepository.saveWord(word2);
        overviewRepository.saveWord(word3);
        overviewRepository.saveWord(word4);
        overviewRepository.saveWord(word5);
        wordLevel = overviewRepository.getListWord();
    }

    @Test
    @WithMockUser
    public void OverviewServiceGetMapBarAndPie() {
        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        ReflectionTestUtils.setField(overviewRepository, "wordLevel", wordLevel);
        ReflectionTestUtils.setField(overviewServiceImpl, "overviewRepository", overviewRepository);
//        OverviewRepository mock = org.mockito.Mockito.mock(OverviewRepository.class);
        overviewServiceImpl.getMapBar();
        overviewServiceImpl.getMapPie();
//        verify(overviewRepository).saveJmlKata("1",  wordLevel.get(0).size());
    }

    @Test
    @WithMockUser
    public void OverviewServiceSaveMapBarAndPie() {
        Users user = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
//        ReflectionTestUtils.setField(overviewRepository, "wordLevel", wordLevel);
        ReflectionTestUtils.setField(overviewServiceImpl, "overviewRepository", overviewRepository);
//        OverviewRepository mock = org.mockito.Mockito.mock(OverviewRepository.class);
        overviewServiceImpl.saveMapBar();
        overviewServiceImpl.saveMapPie();
//        verify(overviewRepository).saveJmlKata("1",  wordLevel.get(0).size());
    }

    @Test
    public void OverviewServiceSaveNullListKata() {
        ReflectionTestUtils.setField(overviewServiceImpl, "overviewRepository", overviewRepository);
//        OverviewRepositoryImpl mock = org.mockito.Mockito.mock(OverviewRepositoryImpl.class);
        overviewServiceImpl.saveNullListKata("bar");
        overviewServiceImpl.saveNullListKata("pie");
//        verify(overviewRepository, times(1)).saveJmlKata("1",0);
//        verify(mock).saveJmlKata("2",  0);
//        verify(mock).saveJmlKata("3",  0);
//        verify(mock).saveJmlKata("4",  0);
//        verify(mock).saveJmlKata("5",  0);
    }
//
    @Test
    public void OverviewServiceLengthWordList() {
        OverviewService mock = org.mockito.Mockito.mock(OverviewService.class);
        lenient().when(mock.lengthWordList()).thenReturn(15);
        Assert.assertEquals(overviewServiceImpl.lengthWordList(),15);
    }
//
//    @Test
//    public void OverviewServiceHitungPersen() {
//        OverviewService mock = org.mockito.Mockito.mock(OverviewService.class);
//        lenient().when(mock.hitungPersen(1, 1)).thenReturn(100);
//        Assert.assertEquals(overviewServiceImpl.hitungPersen(1,1),100);
//    }
}