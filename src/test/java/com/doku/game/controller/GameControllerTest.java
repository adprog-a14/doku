package com.doku.game.controller;

import com.doku.auth.UserRepository;
import com.doku.overview.repository.OverviewRepositoryImpl;
import com.doku.overview.service.OverviewServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = GameController.class)
public class GameControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private String language = "Indonesia";

    @Test
    @WithMockUser
    public void getGameShouldBeOKAndReturnGameHomepageView() throws Exception{
        mockMvc.perform(get("/game"))
                .andExpect(status().isOk())
                .andExpect(view().name("game/GameHomepage"));
    }

    @Test
    @WithMockUser
    public void getGameStartGameShouldBeOKAndReturnGameStartGameView() throws Exception{
        mockMvc.perform(get("/gameLanguage/" + language))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("languages"))
                .andExpect(view().name("game/StartGame"));
    }

}
