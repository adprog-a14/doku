package com.doku.Level.repository;

import com.doku.auth.model.Users;
import com.doku.level.repository.UserWordRepository;
import com.doku.translate.model.EnId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserWordRepositoryTest {
    @Mock
    UserWordRepository userWordRepository;

    private String[] abominablyExpected = new String[]{"", "ks. buruk sekali (of food, style).", ", adverb form of \"abominable\""};

    @Test
    void findWordbyLevel() {
        EnId mockedWord = new EnId("abominable", abominablyExpected[1], abominablyExpected[2]);
        when(userWordRepository.findWordbyLevel(new Users(), 1)).thenReturn(new ArrayList<EnId>() {{add(mockedWord);}});

        List<EnId> result = userWordRepository.findWordbyLevel(new Users(), 1);
        assertEquals(1, result.size());
        assertEquals(result.get(0), mockedWord);
    }
}