package com.doku.Level.service;

import com.doku.auth.UserRepository;
import com.doku.auth.model.Users;
import com.doku.level.model.UserWord;
import com.doku.level.repository.UserWordRepository;
import com.doku.level.service.LevelServiceImpl;
import com.doku.translate.model.EnId;
import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LevelServiceTest {

    @Mock
    UserWordRepository userWordRepository;

    @Mock
    EnIdRepository en_idRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    LevelServiceImpl levelService;

    private String[] abominablyExpected = new String[]{"", "ks. buruk sekali (of food, style).", ", adverb form of \"abominable\""};

    @Test
    public void testAssignLevel() {
        EnId mockedWord = new EnId("abominable", abominablyExpected[1], abominablyExpected[2]);
        when(en_idRepository.findByWord("abominable")).thenReturn(mockedWord);

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(new Users()));
        SecurityContextHolder.setContext(securityContext);

        levelService.assignLevel("abominable", 1);
        verify(userWordRepository, times(1)).save(any());
    }

    @Test
    void testReplaceLevel() {
        Users user = new Users();

        EnId mockedWord = new EnId("abominable", abominablyExpected[1], abominablyExpected[2]);
        when(en_idRepository.findByWord("abominable")).thenReturn(mockedWord);

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(user));
        user.addWord(new UserWord(user, mockedWord, 2));
        SecurityContextHolder.setContext(securityContext);
        levelService.assignLevel("abominable", 1);

        verify(userWordRepository, times(1)).delete(any());
        verify(userWordRepository, times(1)).save(any());
    }

    @Test
    void testGetUserLevel() {
        Users user = new Users();

        EnId mockedWord = new EnId("abominable", abominablyExpected[1], abominablyExpected[2]);
        when(userWordRepository.findWordbyLevel(user, 1)).thenReturn(new ArrayList<EnId>() {{add(mockedWord);}});

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        Mockito.when(authentication.getName()).thenReturn("user1");
        Mockito.when(userRepository.findUserByUsername("user1")).thenReturn(java.util.Optional.of(user));
        SecurityContextHolder.setContext(securityContext);

        Map<String, Integer> result = levelService.getUserLevel();

        assertEquals(1, result.size());
        assertEquals(1, result.get("abominable"));
    }
}