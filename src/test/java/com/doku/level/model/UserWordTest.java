package com.doku.Level.model;

import com.doku.level.model.UserWord;
import com.doku.translate.model.EnId;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserWordTest {
    private UserWord userWord = new UserWord();
    private EnId enId = new EnId("aaa", "aaa");

    @Test
    void setWord() {
        userWord.setWord(enId);
        assertEquals(userWord.getWord(), enId);
    }

    @Test
    void setLevel() {
        userWord.setLevel(1);
        assertEquals(userWord.getLevel(), 1);
    }

    @Test
    void getWord() {
    }

    @Test
    void getLevel() {
    }

    @Test
    void testToString() {
        userWord.setWord(enId);
        userWord.setLevel(1);
        assertEquals(userWord.toString(),  "null " + userWord.getWord() + " " + userWord.getLevel());
    }
}