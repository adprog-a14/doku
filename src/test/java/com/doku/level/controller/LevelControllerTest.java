package com.doku.Level.controller;

import com.doku.auth.UserRepository;
import com.doku.level.controller.LevelController;
import com.doku.level.repository.UserWordRepository;
import com.doku.level.service.LevelService;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LevelController.class)
class LevelControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserWordRepository userWordRepository;

    @MockBean
    private LevelService levelService;

    @Test
    @WithMockUser
    public void testAssignLevelSucceed() throws Exception {
        mvc.perform(post("/assignLevel/1").contentType(MediaType.APPLICATION_JSON).content("lion"))
                .andExpect(status().isOk());
    }


    @Test
    @WithMockUser
    void testGetUserLevel() throws Exception {
        mvc.perform(get("/getUserLevel"))
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}