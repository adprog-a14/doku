package com.doku.comment.controller;

import com.doku.auth.UserRepository;
import com.doku.comment.model.Comment;
import com.doku.comment.repository.CommentRepository;
import com.doku.comment.service.CommentServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.*;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = ViewCommentController.class)
public class ViewCommentControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CommentServiceImpl commentService;

    private final int ID_Comment_NEW = 3;

    private Comment comment;

    @BeforeEach
    public void setUp() {
        comment = new Comment(ID_Comment_NEW,"test",0,"ini adalah test!");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser
    public void testControllerGetLog() throws Exception {
        when(commentService.newComment("ini adalah test!","test")).thenReturn(comment);

        mvc.perform(post("/input-comment/" + "test")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(comment)));
    }
}
