package com.doku.comment.service;

import com.doku.comment.repository.CommentRepository;
import com.doku.comment.model.Comment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTest {

    @Mock
    private CommentRepository commentRepository;

    @InjectMocks
    private CommentServiceImpl commentService;

    private final int ID_Comment_NEW = 3;

    private Comment comment;

    @BeforeEach
    public void setUp() {
        comment = new Comment(ID_Comment_NEW,"test",0,"ini adalah test!");
    }

    @Test
    public void testServiceCreateComment(){
        commentService.createComment(comment);
        verify(commentRepository,times(1)).save(comment);
    }

    @Test
    public void testServiceCreateDuplicateCommentShouldFail() {
        when(commentRepository.findById(comment.getIdComment())).thenReturn(comment);
        commentService.createComment(comment);
        verify(commentRepository, times(0)).save(comment);
    }

    @Test
    public void testServiceGetListComment(){
        Iterable<Comment> listComment = commentRepository.findAll();
        lenient().when(commentService.getListComment("test")).thenReturn(listComment);
        Iterable<Comment> listCommentResult = commentService.getListComment("test");
        Assertions.assertIterableEquals(listComment, listCommentResult);
    }

    @Test
    public void testServiceVoteUp(){
        commentService.createComment(comment);
        int currentVote = comment.getVote();
        //Change description
        comment.setVote(comment.getVote()+1);

        commentService.voteUp(comment.getIdComment());
        assertNotEquals(comment.getVote(), currentVote);
        assertEquals(comment.getVote(), comment.getVote());
    }

    @Test
    public void testServiceVoteDown(){
        commentService.createComment(comment);
        int currentVote = comment.getVote();
        //Change description
        comment.setVote(comment.getVote()-1);

        commentService.voteDown(comment.getIdComment());

        assertNotEquals(comment.getVote(), currentVote);
        assertEquals(comment.getVote(), comment.getVote());
    }
}
