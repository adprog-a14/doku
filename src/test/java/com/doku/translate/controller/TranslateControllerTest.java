package com.doku.translate.controller;

import com.doku.auth.UserRepository;
import com.doku.translate.service.EnIdService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = TranslateController.class)
class TranslateControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private EnIdService en_idService;

    private String word;
    private String[] trans;

    @BeforeEach
    public void setUp() {
        word = "Accountant.";
        trans = new String[]{"accountant", "kb. akuntan.", ""};
    }

    @Test
    @WithMockUser
    void postTranslateShouldReturnSuccess() throws Exception {
        when(en_idService.getTransbyWord(word)).thenReturn(trans);

        mvc.perform(post("/translate").contentType(MediaType.APPLICATION_JSON).content(word))
                .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

    }
}