package com.doku.translate.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class ParserFactoryTest {
    @Autowired
    private ParserFactory parserFactory;

    @BeforeEach
    void setup() {
        parserFactory.initialize();
    }

    @Test
    void testGetParserCheckAdverb() {
        assertEquals(parserFactory.getParser("CheckAdverb").toString(), "CheckAdverbParser");
    }

    @Test
    void testGetParserCheckPlural() {
        assertEquals(parserFactory.getParser("CheckPlural").toString(), "CheckPluralParser");
    }

    @Test
    void testGetParserCheckVerbTwo() {
        assertEquals(parserFactory.getParser("CheckVerbTwo").toString(), "CheckVerbTwoParser");
    }

    @Test
    void testGetParserCheckIng() {
        assertEquals(parserFactory.getParser("CheckIng").toString(), "CheckIngParser");
    }

    @Test
    void testGetParserCheckLative() {
        assertEquals(parserFactory.getParser("CheckLative").toString(), "CheckLativeParser");
    }

    @Test
    void testGetLihParserAwal() {
        assertEquals(parserFactory.getParser("LihParserAwal").toString(), "LihParserAwal");
    }

    @Test
    void testGetLihParserMid() {
        assertEquals(parserFactory.getParser("LihParserMid").toString(), "LihParserMid");
    }
}