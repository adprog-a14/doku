package com.doku.translate.model;

import com.doku.level.model.UserWord;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class En_IdTest {
    private EnId enIdEmpty = new EnId();
    private EnId enId = new EnId("aaa", "test");
    private UserWord oldUW = new UserWord();
    private UserWord newUW = new UserWord();

    @Test
    void getWord() {
    }

    @Test
    void setWord() {
        enId.setWord("iii");
        assertEquals(enId.getWord(), "iii");
        enId.setWord("aaa");
    }

    @Test
    void getTrans() {
    }

    @Test
    void setTrans() {
    }

    @Test
    void setUsers() {
    }

    @Test
    void setBookmarked() {
    }

    @Test
    void addUser() {
        enId.addUser(oldUW);
        assertTrue(enId.checkUser(oldUW));
    }

    @Test
    void replaceWord() {
        enId.addUser(oldUW);
        enId.replaceWord(oldUW, newUW);
        assertTrue(enId.checkUser(newUW));
        assertFalse(enId.checkUser(oldUW));
    }

    @Test
    void testToString() {
        assertEquals(enId.toString(), "aaa : test");
    }
}