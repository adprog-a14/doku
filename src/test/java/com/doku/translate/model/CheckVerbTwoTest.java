package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckVerbTwoTest {

    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    CheckVerbTwo checkVerbTwo;

    private String[] bakedExpected = new String[] {"", "kb. pesta atau pertemuan informil dimana diadakan makan. -kkt. membakar (a cake). -kki. matang.", ", past participle form of \"bake\""};
    private String[] dreadedExpected = new String[] {"", "kb. rasa takut, ketakutan. -kkt. takut (kepada).", ", past participle form of \"dread\""};
    private String[] gaggedExpected = new String[] {"", "kb. Sl.: 1 lelucon.2 sumbat. -kkt.(gagged) menyumbat.-kki.1 tercekik. 2 muntah", ", past participle form of \"gag\""};
    private String[] deniedExpected = new String[] {"", "kkt. (denied) 1 menyangkal, mengingkari. 2 meniadakan.", ", past participle form of \"deny\""};
    private String[] notFound = new String[] {"", "Kata tidak dapat ditemukan di kamus", ""};

    @Test
    void testParseCaseRemoveIng() {
        EnId mockedWord = new EnId("bake", bakedExpected[1], bakedExpected[2]);
        when(en_idRepository.findByWord("bake")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("bake")))).thenReturn(null);

        String[] actual = checkVerbTwo.parse("baked");
        assertEquals(actual[0], bakedExpected[0]);
        assertEquals(actual[1], bakedExpected[1]);
        assertEquals(actual[2], bakedExpected[2]);
    }

    @Test
    void testParseCaseRemoveEd() {
        EnId mockedWord = new EnId("dread", dreadedExpected[1], dreadedExpected[2]);
        when(en_idRepository.findByWord("dread")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("dread")))).thenReturn(null);

        String[] actual = checkVerbTwo.parse("dreaded");
        assertEquals(actual[0], dreadedExpected[0]);
        assertEquals(actual[1], dreadedExpected[1]);
        assertEquals(actual[2], dreadedExpected[2]);
    }

    @Test
    void testParseCaseChangeIngToIe() {
        EnId mockedWord = new EnId("gag", gaggedExpected[1], gaggedExpected[2]);
        when(en_idRepository.findByWord("gag")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("gag")))).thenReturn(null);

        String[] actual = checkVerbTwo.parse("gagged");
        assertEquals(actual[0], gaggedExpected[0]);
        assertEquals(actual[1], gaggedExpected[1]);
        assertEquals(actual[2], gaggedExpected[2]);
    }

    @Test
    void testParseCaseRemoveLastFour() {
        EnId mockedWord = new EnId("deny", deniedExpected[1], deniedExpected[2]);
        when(en_idRepository.findByWord("deny")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("deny")))).thenReturn(null);

        String[] actual = checkVerbTwo.parse("denied");
        assertEquals(actual[0], deniedExpected[0]);
        assertEquals(actual[1], deniedExpected[1]);
        assertEquals(actual[2], deniedExpected[2]);
    }

    @Test
    void testParseCaseNotFound() {
        String[] actual = checkVerbTwo.parse("dfsada");
        assertEquals(actual[0], notFound[0]);
        assertEquals(actual[1], notFound[1]);
        assertEquals(actual[2], notFound[2]);
    }
}