package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckIngTest {
    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    CheckIng checkIng;

    private String[] cheatingExpected = new String[] {"", "kb. penipu. -kkt. menipu. -kki. 1 menyontek, menjiplak. 2 main curang.  -cheating kb. menyontek, menjiplak.", ", present participle or gerund form of \"cheat\""};
    private String[] samplingExpected = new String[] {"", "kb. contoh.  -kkt. 1 mencicipi, mengecap (food). 2 mengalami (the experiences of life). 3 mencoba (a new restaurant). -sampling kb. penarikan contoh.", ", present participle or gerund form of \"sample\""};
    private String[] untyingExpected = new String[] {"", "kkt. membuka (tali). menguraikan. to come untied lepas.", ", present participle or gerund form of \"untie\""};
    private String[] chattingExpected = new String[] {"", "kb. obrolan.  to get together a c. bertemu untuk ngobrol. -kki. (chatted) mengobrol, bercakap-cakap.", ", present participle or gerund form of \"chat\""};
    private String[] notFound = new String[] {"", "Kata tidak dapat ditemukan di kamus", ""};

    @Test
    void testParseCaseRemoveIng() {
        EnId mockedWord = new EnId("cheat", cheatingExpected[1], cheatingExpected[2]);
        when(en_idRepository.findByWord("cheat")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("cheat")))).thenReturn(null);

        String[] actual = checkIng.parse("cheating");
        assertEquals(actual[0], cheatingExpected[0]);
        assertEquals(actual[1], cheatingExpected[1]);
        assertEquals(actual[2], cheatingExpected[2]);
    }

    @Test
    void testParseCaseChangeIngToE() {
        EnId mockedWord = new EnId("sample", samplingExpected[1], samplingExpected[2]);
        when(en_idRepository.findByWord("sample")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("sample")))).thenReturn(null);

        String[] actual = checkIng.parse("sampling");
        assertEquals(actual[0], samplingExpected[0]);
        assertEquals(actual[1], samplingExpected[1]);
        assertEquals(actual[2], samplingExpected[2]);
    }

    @Test
    void testParseCaseChangeIngToIe() {
        EnId mockedWord = new EnId("untie", untyingExpected[1], untyingExpected[2]);
        when(en_idRepository.findByWord("untie")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("untie")))).thenReturn(null);

        String[] actual = checkIng.parse("untying");
        assertEquals(actual[0], untyingExpected[0]);
        assertEquals(actual[1], untyingExpected[1]);
        assertEquals(actual[2], untyingExpected[2]);
    }

    @Test
    void testParseCaseRemoveLastFour() {
        EnId mockedWord = new EnId("chat", chattingExpected[1], chattingExpected[2]);
        when(en_idRepository.findByWord("chat")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("chat")))).thenReturn(null);

        String[] actual = checkIng.parse("chatting");
        assertEquals(actual[0], chattingExpected[0]);
        assertEquals(actual[1], chattingExpected[1]);
        assertEquals(actual[2], chattingExpected[2]);
    }

    @Test
    void testParseCaseNotFound() {
        String[] actual = checkIng.parse("dfsada");
        assertEquals(actual[0], notFound[0]);
        assertEquals(actual[1], notFound[1]);
        assertEquals(actual[2], notFound[2]);
    }
}