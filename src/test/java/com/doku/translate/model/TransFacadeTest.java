package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransFacadeTest {
    @Mock
    EnIdRepository en_idRepository;

    @Mock
    ParserFactory parserFactory;

    @Mock
    ParseStrategy parseStrategy;

    @InjectMocks
    private TransFacade transFacade;

    private String[] lionExpected = new String[] {"lion", "kb. singa. to beard the l. in his den mendatangi seseorang di tempatnya.", null};
    private String[] tragicallyExpected = new String[] {"tragically", "ks. tragis.", ", adverb form of \"tragic\""};
    private String[] untyingExpected = new String[] {"untying", "kkt. membuka (tali). menguraikan. to come untied lepas.", ", present participle or gerund form of \"untie\""};
    private String[] skinnierExpected = new String[] {"skinnier", "ks. kurus.", ", comparative form of \"skinny\""};
    private String[] marshesExpected = new String[] {"marshes", "kb. rawa, paya.", ", plural form of \"marsh\""};
    private String[] dreadedExpected = new String[] {"dreaded", "kb. rasa takut, ketakutan. -kkt. takut (kepada).", ", past participle form of \"dread\""};
    private String[] bereftExpected = new String[] {"bereft", "kkt. (bereaved atau bereft) kehilangan. He was bereft of hope Dia kehilangan harapan.", ", irregular form of \"bereave\""};
    private String[] heartilyExpected = new String[] {"heartily", "kk.  ks. 1 besar. 2 sungguh-sungguh, dengan sepenuh hati. -heartily kk. 1 dengan penuh nafsu makan. 2 sungguh-sungguh. to disagree h. sungguh-sungguh tidak setuju..", ", irregular form of \"hearty\""};
    private String[] notFound = new String[] {"dsasd", "Kata tidak dapat ditemukan di kamus", ""};

    private EnId heartily = new EnId("heartily", "kk. lih  HEARTY.");
    private EnId bereft = new EnId("bereft", "lih BEREAVE.");

    @Test
    void testNoParse() {
        EnId mockedWord = new EnId("lion", lionExpected[1], lionExpected[2]);
        when(en_idRepository.findByWord("lion")).thenReturn(mockedWord);

        String[] actual = transFacade.translate("lion");
        assertEquals(actual[0], lionExpected[0]);
        assertEquals(actual[1], lionExpected[1]);
        assertEquals(actual[2], lionExpected[2]);
        assertEquals(actual[1], en_idRepository.findByWord("lion").getTrans());
        assertEquals(actual[2], en_idRepository.findByWord("lion").getNote());
    }

    @Test
    void testGetParserCheckAdverb() {
        when(parseStrategy.parse("tragically")).thenReturn(tragicallyExpected);

        String[] actual = transFacade.translate("tragically");
        assertEquals(actual[0], tragicallyExpected[0]);
        assertEquals(actual[1], tragicallyExpected[1]);
        assertEquals(actual[2], tragicallyExpected[2]);
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testParseCaseChangeIngToIe() { ;
        when(parseStrategy.parse("untying")).thenReturn(untyingExpected);

        String[] actual = transFacade.translate("untying");
        assertEquals(actual[0], untyingExpected[0]);
        assertEquals(actual[1], untyingExpected[1]);
        assertEquals(actual[2], untyingExpected[2]);
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testParseCaseComparativeIer() {
        when(parseStrategy.parse("skinnier")).thenReturn(skinnierExpected);

        String[] actual = transFacade.translate("skinnier");
        assertEquals(actual[0], skinnierExpected[0]);
        assertEquals(actual[1], skinnierExpected[1]);
        assertEquals(actual[2], skinnierExpected[2]);
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testParseRemoveEs() {
        when(parseStrategy.parse("marshes")).thenReturn(marshesExpected);

        String[] actual = transFacade.translate("marshes");
        assertEquals(actual[0], marshesExpected[0]);
        assertEquals(actual[1], marshesExpected[1]);
        assertEquals(actual[2], marshesExpected[2]);
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testParseCaseRemoveEd() {
        when(parseStrategy.parse("dreaded")).thenReturn(dreadedExpected);

        String[] actual = transFacade.translate("dreaded");
        assertEquals(actual[0], dreadedExpected[0]);
        assertEquals(actual[1], dreadedExpected[1]);
        assertEquals(actual[2], dreadedExpected[2]);
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testAwalLihParse() {
        when(parseStrategy.parse("lih BEREAVE.")).thenReturn(bereftExpected);
        EnId mockedWord = new EnId("bereave", "lih BEREAVE.", null);
        when(en_idRepository.findByWord("bereft")).thenReturn(mockedWord);

        String[] actual = transFacade.translate("bereft");
        assertEquals(actual[0], bereftExpected[0]);
        assertEquals(actual[1], bereftExpected[1]);
        assertEquals(actual[2], bereftExpected[2]);
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testMidLihParse() {
        when(parseStrategy.parse("kk. lih  HEARTY.")).thenReturn(heartilyExpected);
        EnId mockedWord = new EnId("hearty", "kk. lih  HEARTY.", null);
        when(en_idRepository.findByWord("heartily")).thenReturn(mockedWord);

        String[] actual = transFacade.translate("heartily");
        assertEquals(actual[0], heartilyExpected[0]);
        assertEquals(actual[1], heartilyExpected[1]);
        assertEquals(actual[2], heartilyExpected[2]);
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testNotFound() {
        String[] actual = transFacade.translate("dsasd");
        assertEquals(actual[0], notFound[0]);
        assertEquals(actual[1], notFound[1]);
        assertEquals(actual[2], notFound[2]);
    }

}