package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckPluralTest {
    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    CheckPlural checkPlural;

    private String[] catsExpected = new String[] {"", "kb. kucing. cat's paw kaki kucing. c. nap tertidur sebentar-sebentar.", ", plural form of \"cat\""};
    private String[] marshesExpected = new String[] {"", "kb. rawa, paya.", ", plural form of \"marsh\""};
    private String[] gassesExpected = new String[] {"", "kb. 1 gas. 2 Inf.: bensin, minyak gas. 3 Med.: obat bius. -kkt. 1 membubuhkan gas. 2 Mil.: menyerang dengan gas beracun. to g. up mengisi bensin sampai penuh. g. burner  kompor gas.", ", plural form of \"gas\""};
    private String[] wolvesExpected = new String[] {"", "kb. 1 serigala. 2 buaya (lelaki), mata keranjang. -kkt. to w. down memakan dengan rakus.", ", plural form of \"wolf\""};
    private String[] afterlivesExpected = new String[] {"", "kb. akhirat, alam baka.", ", plural form of \"afterlife\""};
    private String[] puppiesExpected = new String[] {"", "kb. (j. -pies) anak anjing. p. love cinta remaja.", ", plural form of \"puppy\""};
    private String[] analysesExpected = new String[] {"", "kb. (j. -ses) 1 analisa, pemisahan. 2 pemeriksaan yang teliti. in the final a. pada hakekatnya. In the final a. it is the performance that  counts Pada hakekatnya yang terpenting ialah penyelenggaraan.", ", plural form of \"analysis\""};
    private String[] notFound = new String[] {"", "Kata tidak dapat ditemukan di kamus", ""};

    @Test
    void testParseRemoveS() {
        EnId mockedWord = new EnId("cat", catsExpected[1], catsExpected[2]);
        when(en_idRepository.findByWord("cat")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("cat")))).thenReturn(null);

        String[] actual = checkPlural.parse("cats");
        assertEquals(actual[0], catsExpected[0]);
        assertEquals(actual[1], catsExpected[1]);
        assertEquals(actual[2], catsExpected[2]);
    }

    @Test
    void testParseRemoveEs() {
        EnId mockedWord = new EnId("marsh", marshesExpected[1], marshesExpected[2]);
        when(en_idRepository.findByWord("marsh")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("marsh")))).thenReturn(null);

        String[] actual = checkPlural.parse("marshes");
        assertEquals(actual[0], marshesExpected[0]);
        assertEquals(actual[1], marshesExpected[1]);
        assertEquals(actual[2], marshesExpected[2]);
    }

    @Test
    void testParseMinThree() {
        EnId mockedWord = new EnId("gas", gassesExpected[1], gassesExpected[2]);
        when(en_idRepository.findByWord("gas")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("gas")))).thenReturn(null);

        String[] actual = checkPlural.parse("gasses");
        assertEquals(actual[0], gassesExpected[0]);
        assertEquals(actual[1], gassesExpected[1]);
        assertEquals(actual[2], gassesExpected[2]);
    }

    @Test
    void testParseForEndingF() {
        EnId mockedWord = new EnId("wolf", wolvesExpected[1], wolvesExpected[2]);
        when(en_idRepository.findByWord("wolf")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("wolf")))).thenReturn(null);

        String[] actual = checkPlural.parse("wolves");
        assertEquals(actual[0], wolvesExpected[0]);
        assertEquals(actual[1], wolvesExpected[1]);
        assertEquals(actual[2], wolvesExpected[2]);
    }

    @Test
    void testParseForEndingFe() {
        EnId mockedWord = new EnId("afterlife", afterlivesExpected[1], afterlivesExpected[2]);
        when(en_idRepository.findByWord("afterlife")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("afterlife")))).thenReturn(null);

        String[] actual = checkPlural.parse("afterlives");
        assertEquals(actual[0], afterlivesExpected[0]);
        assertEquals(actual[1], afterlivesExpected[1]);
        assertEquals(actual[2], afterlivesExpected[2]);
    }

    @Test
    void testParseForEndingY() {
        EnId mockedWord = new EnId("puppy", puppiesExpected[1], puppiesExpected[2]);
        when(en_idRepository.findByWord("puppy")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("puppy")))).thenReturn(null);

        String[] actual = checkPlural.parse("puppies");
        assertEquals(actual[0], puppiesExpected[0]);
        assertEquals(actual[1], puppiesExpected[1]);
        assertEquals(actual[2], puppiesExpected[2]);
    }

    @Test
    void testParseForEndingSis() {
        EnId mockedWord = new EnId("analysis", analysesExpected[1], analysesExpected[2]);
        when(en_idRepository.findByWord("analysis")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("analysis")))).thenReturn(null);

        String[] actual = checkPlural.parse("analyses");
        assertEquals(actual[0], analysesExpected[0]);
        assertEquals(actual[1], analysesExpected[1]);
        assertEquals(actual[2], analysesExpected[2]);
    }

    @Test
    void testParseCaseNotFound() {
        String[] actual = checkPlural.parse("dfsada");
        assertEquals(actual[0], notFound[0]);
        assertEquals(actual[1], notFound[1]);
        assertEquals(actual[2], notFound[2]);
    }
}