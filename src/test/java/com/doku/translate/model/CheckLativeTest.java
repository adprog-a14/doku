package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckLativeTest {
    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    CheckLative checkLative;

    private String[] darkerExpected = new String[] {"", "kb. kegelapan.  after d.  waktu malam. -ks. 1 gelap. 2 tua. 3 suram.  dark-skinned ks. berkulit hitam.", ", comparative form of \"dark\""};
    private String[] darkestExpected = new String[] {"", "kb. kegelapan.  after d.  waktu malam. -ks. 1 gelap. 2 tua. 3 suram.  dark-skinned ks. berkulit hitam.", ", superlative form of \"dark\""};
    private String[] cuterExpected = new String[] {"", "ks. Inf.: 1 mungil, manis. c. baby bayi yang mungil.  2  elok.  She has a c. figure  Badannya elok sekali.", ", comparative form of \"cute\""};
    private String[] cutestExpected = new String[] {"", "ks. Inf.: 1 mungil, manis. c. baby bayi yang mungil.  2  elok.  She has a c. figure  Badannya elok sekali.", ", superlative form of \"cute\""};
    private String[] skinnierExpected = new String[] {"", "ks. kurus.", ", comparative form of \"skinny\""};
    private String[] skinniestExpected = new String[] {"", "ks. kurus.", ", superlative form of \"skinny\""};
    private String[] notFound = new String[] {"", "Kata tidak dapat ditemukan di kamus", ""};

    @Test
    void testParseComparativeEr() {
        EnId mockedWord = new EnId("dark", darkerExpected[1], darkerExpected[2]);
        when(en_idRepository.findByWord("dark")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("dark")))).thenReturn(null);

        String[] actual = checkLative.parse("darker");
        assertEquals(actual[0], darkerExpected[0]);
        assertEquals(actual[1], darkerExpected[1]);
        assertEquals(actual[2], darkerExpected[2]);
    }

    @Test
    void testParseSuperlativeEst() {
        EnId mockedWord = new EnId("dark", darkestExpected[1], darkestExpected[2]);
        when(en_idRepository.findByWord("dark")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("dark")))).thenReturn(null);

        String[] actual = checkLative.parse("darkest");
        assertEquals(actual[0], darkestExpected[0]);
        assertEquals(actual[1], darkestExpected[1]);
        assertEquals(actual[2], darkestExpected[2]);
    }

    @Test
    void testParseCaseComparativeR() {
        EnId mockedWord = new EnId("cute", cuterExpected[1], cuterExpected[2]);
        when(en_idRepository.findByWord("cute")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("cute")))).thenReturn(null);

        String[] actual = checkLative.parse("cuter");
        assertEquals(actual[0], cuterExpected[0]);
        assertEquals(actual[1], cuterExpected[1]);
        assertEquals(actual[2], cuterExpected[2]);
    }

    @Test
    void testParseCaseSuperlativeEst() {
        EnId mockedWord = new EnId("cute", cutestExpected[1], cutestExpected[2]);
        when(en_idRepository.findByWord("cute")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("cute")))).thenReturn(null);

        String[] actual = checkLative.parse("cutest");
        assertEquals(actual[0], cutestExpected[0]);
        assertEquals(actual[1], cutestExpected[1]);
        assertEquals(actual[2], cutestExpected[2]);
    }

    @Test
    void testParseCaseComparativeIer() {
        EnId mockedWord = new EnId("skinny", skinnierExpected[1], skinnierExpected[2]);
        when(en_idRepository.findByWord("skinny")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("skinny")))).thenReturn(null);

        String[] actual = checkLative.parse("skinnier");
        assertEquals(actual[0], skinnierExpected[0]);
        assertEquals(actual[1], skinnierExpected[1]);
        assertEquals(actual[2], skinnierExpected[2]);
    }

    @Test
    void testParseCaseSuperlativeIest() {
        EnId mockedWord = new EnId("skinny", skinniestExpected[1], skinniestExpected[2]);
        when(en_idRepository.findByWord("skinny")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("skinny")))).thenReturn(null);

        String[] actual = checkLative.parse("skinniest");
        assertEquals(actual[0], skinniestExpected[0]);
        assertEquals(actual[1], skinniestExpected[1]);
        assertEquals(actual[2], skinniestExpected[2]);
    }

    @Test
    void testParseCaseNotFound() {
        String[] actual = checkLative.parse("dfsada");
        assertEquals(actual[0], notFound[0]);
        assertEquals(actual[1], notFound[1]);
        assertEquals(actual[2], notFound[2]);
    }
}