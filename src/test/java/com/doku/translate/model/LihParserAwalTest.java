package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LihParserAwalTest {

    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    LihParserAwal lihParserAwal;

    private String[] bereftExpected = new String[] {"", "kkt. (bereaved atau bereft) kehilangan. He was bereft of hope Dia kehilangan harapan.", ", irregular form of \"bereave\""};

    @Test
    void testParseCaseRemoveIng() {
        EnId mockedWord = new EnId("bereave", bereftExpected[1], bereftExpected[2]);
        when(en_idRepository.findByWord("bereave")).thenReturn(mockedWord);

        String[] actual = lihParserAwal.parse("lih BEREAVE.");
        assertEquals(actual[0], bereftExpected[0]);
        assertEquals(actual[1], bereftExpected[1]);
        assertEquals(actual[2], bereftExpected[2]);
    }
}