package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckAdverbTest {

    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    CheckAdverb checkAdverb;

    private String[] abominablyExpected = new String[] {"", "ks. buruk sekali (of food, style).", ", adverb form of \"abominable\""};
    private String[] extravagantlyExpected = new String[] {"", "ks. ks. boros, royal. 2 luarbiasa. 3 yang berlebih-lebihan (praise). -extravagantly kk. secara mewah sekali.", ", adverb form of \"extravagant\""};
    private String[] tragicallyExpected = new String[] {"", "ks. tragis.", ", adverb form of \"tragic\""};
    private String[] angrilyExpected = new String[] {"", "ks. 1 marah, gusar, naik darah. Why are you so a.? Mengapa kau begitu marah?  a. at/with marah pada/dgn. 2 besar. That's an a. sore you have on your thigh Itu luka besar yang ada di pahamu. 3 bergejolak. the a. sea laut yang besar ombaknya.", ", adverb form of \"angry\""};
    private String[] notFound = new String[] {"", "Kata tidak dapat ditemukan di kamus", ""};

    @Test
    void testParseCaseChangeYtoE() {
        EnId mockedWord = new EnId("abominable", abominablyExpected[1], abominablyExpected[2]);
        when(en_idRepository.findByWord("abominable")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("abominable")))).thenReturn(null);

        String[] actual = checkAdverb.parse("abominably");
        assertEquals(actual[0], abominablyExpected[0]);
        assertEquals(actual[1], abominablyExpected[1]);
        assertEquals(actual[2], abominablyExpected[2]);
    }

    @Test
    void testParseCaseRemoveLy() {
        EnId mockedWord = new EnId("extravagant", extravagantlyExpected[1], extravagantlyExpected[2]);
        when(en_idRepository.findByWord("extravagant")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("extravagant")))).thenReturn(null);

        String[] actual = checkAdverb.parse("extravagantly");
        assertEquals(actual[0], extravagantlyExpected[0]);
        assertEquals(actual[1], extravagantlyExpected[1]);
        assertEquals(actual[2], extravagantlyExpected[2]);
    }

    @Test
    void testParseCaseRemoveAlly() {
        EnId mockedWord = new EnId("tragic", tragicallyExpected[1], tragicallyExpected[2]);
        when(en_idRepository.findByWord("tragic")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("tragic")))).thenReturn(null);

        String[] actual = checkAdverb.parse("tragically");
        assertEquals(actual[0], tragicallyExpected[0]);
        assertEquals(actual[1], tragicallyExpected[1]);
        assertEquals(actual[2], tragicallyExpected[2]);
    }

    @Test
    void testParseCaseChangeIlyToY() {
        EnId mockedWord = new EnId("angry", angrilyExpected[1], angrilyExpected[2]);
        when(en_idRepository.findByWord("angry")).thenReturn(mockedWord);
        when(en_idRepository.findByWord(not(eq("angry")))).thenReturn(null);

        String[] actual = checkAdverb.parse("angrily");
        assertEquals(actual[0], angrilyExpected[0]);
        assertEquals(actual[1], angrilyExpected[1]);
        assertEquals(actual[2], angrilyExpected[2]);
    }

    @Test
    void testParseCaseNotFound() {
        String[] actual = checkAdverb.parse("dfsada");
        assertEquals(actual[0], notFound[0]);
        assertEquals(actual[1], notFound[1]);
        assertEquals(actual[2], notFound[2]);
    }
}