package com.doku.translate.model;

import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LihParserMidTest {
    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    LihParserMid lihParserMid;

    private String[] heartilyExpected = new String[] {"", "kk.  ks. 1 besar. 2 sungguh-sungguh, dengan sepenuh hati. -heartily kk. 1 dengan penuh nafsu makan. 2 sungguh-sungguh. to disagree h. sungguh-sungguh tidak setuju..", ", irregular form of \"hearty\""};

    @Test
    void testMidLihParse() {
        EnId mockedWord = new EnId("hearty", heartilyExpected[1], heartilyExpected[2]);
        when(en_idRepository.findByWord("hearty")).thenReturn(mockedWord);

        String[] actual = lihParserMid.parse("kk. lih  HEARTY.");
        assertEquals(actual[0], heartilyExpected[0]);
        assertEquals(actual[1], "kk.  kk.  ks. 1 besar. 2 sungguh-sungguh, dengan sepenuh hati. -heartily kk. 1 dengan penuh nafsu makan. 2 sungguh-sungguh. to disagree h. sungguh-sungguh tidak setuju...");
        assertEquals(actual[2], heartilyExpected[2]);
    }
}