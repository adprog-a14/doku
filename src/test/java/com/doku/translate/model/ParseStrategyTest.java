package com.doku.translate.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaserStrategyTest {
    @Mock
    private Parser parser;

    @InjectMocks
    private ParseStrategy parseStrategy;

    @Mock
    private ParserFactory parserFactory;

    private String[] tragicallyExpected = new String[] {"", "ks. tragis.", ", adverb form of \"tragic\""};

    @Test
    void testGetParser() {
        assertEquals(parseStrategy.getParser().toString(), "parser");
    }

    @Test
    void testGetParserCheckAdverb() {
        when(parser.parse("tragically")).thenReturn(tragicallyExpected);

        String[] actual = parseStrategy.parse("tragically");
        assertEquals(actual[0], tragicallyExpected[0]);
        assertEquals(actual[1], tragicallyExpected[1]);
        assertEquals(actual[2], tragicallyExpected[2]);
    }
}