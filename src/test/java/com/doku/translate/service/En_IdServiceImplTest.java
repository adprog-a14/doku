package com.doku.translate.service;

import com.doku.translate.model.*;
import com.doku.translate.repository.EnIdRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class En_IdServiceImplTest {
    @Mock
    EnIdRepository en_idRepository;

    @InjectMocks
    private EnIdServiceImpl en_idService;

    @Mock
    private TransFacade transFacade;

    @Spy
    private ParserFactory parserFactory;

    @Spy
    ApplicationContext context;

    private String[] marshesExpected = new String[] {"marshes", "kb. rawa, paya.", ", plural form of \"marsh\""};

    @Test
    void testAddWord() {
        en_idService.addWord("datesuto", "fo datesuto");
        verify(en_idRepository, times(1)).save(any());
    }

    @Test
    void testGetTransbyWord() {
        given(transFacade.translate("marshes")).willReturn(marshesExpected);

        String[] actual = en_idService.getTransbyWord("marshes");
        assertEquals(actual[0], marshesExpected[0]);
        assertEquals(actual[1], marshesExpected[1]);
        assertEquals(actual[2], marshesExpected[2]);
        en_idRepository.deleteById("marshes");
    }

    @AfterEach
    void cleanUp() {
        if (en_idRepository.findByWord("datesuto") != null) {
            en_idRepository.deleteById("datesuto");
        }
    }
}