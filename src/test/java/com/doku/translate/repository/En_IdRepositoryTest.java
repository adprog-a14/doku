package com.doku.translate.repository;

import com.doku.translate.model.EnId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class En_IdRepositoryTest {
    @Mock
    EnIdRepository enIdRepository;

    private String[] bereftExpected = new String[] {"bereft", "lih BEREAVE.", null};

    @Test
    void testFindByWord() {
        when(enIdRepository.findByWord("bereft")).thenReturn(new EnId(bereftExpected[0], bereftExpected[1]));

        EnId bereft = enIdRepository.findByWord("bereft");
        assertEquals(bereft.getWord(), bereftExpected[0]);
        assertEquals(bereft.getTrans(), bereftExpected[1]);
        assertEquals(bereft.getNote(), bereftExpected[2]);
    }
}