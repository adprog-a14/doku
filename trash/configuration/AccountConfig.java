package com.doku.configuration;

import com.doku.model.Account;
import com.doku.repository.AccountRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class AccountConfig {

    @Bean
    CommandLineRunner commandLineRunner(AccountRepository repository){
        return args -> {
            Account account = new Account(
                    "user",
                    "user"
            );

            repository.saveAll(
                    List.of(account)
            );
        };
    }
}
