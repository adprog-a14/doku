#### Master Coverage / Pipeline
[![pipeline status](https://gitlab.com/adprog-a14/doku/badges/master/pipeline.svg)](https://gitlab.com/adprog-a14/doku/-/commits/master)
[![coverage report](https://gitlab.com/adprog-a14/doku/badges/master/coverage.svg)](https://gitlab.com/adprog-a14/doku/-/commits/master)

## TK Advanced Programming Kelompok A14

| No |      Nama      |     NPM    |     Fitur     |
|:--:|:--------------:|:----------:|:-------------:|
|  1 | Aldi | 1906360635 | Front-end Game Tebak-tebakan, Comment |
|  2 | Muhammad Aulia Adil Murtito | 1906398591 | Back-end Game Tebak-tebakan, Authentication |
|  3 | Muhammad Hanif Fahreza  | 1906351026 | PDF Translate Level |
|  4 | Najmi Aliya  | 1906398622 | Overview |

## **DOKU**

Website aplikasi untuk belajar bahasa Inggris melalui metode membaca, terutama untuk meningkatkan tingkat kosa kata
dan secara alami memahami tata bahasa.

## Status Fitur

#### **[Authentication](https://gitlab.com/adprog-a14/doku/-/tree/Auth_JPA)**
[![pipeline status](https://gitlab.com/adprog-a14/doku/badges/Auth_JPA/pipeline.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Auth_JPA)
[![coverage report](https://gitlab.com/adprog-a14/doku/badges/Auth_JPA/coverage.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Auth_JPA)

#### **[Comment](https://gitlab.com/adprog-a14/doku/-/tree/gameAndComment)**
[![pipeline status](https://gitlab.com/adprog-a14/doku/badges/gameAndComment/pipeline.svg)](https://gitlab.com/adprog-a14/doku/-/commits/gameAndComment)
[![coverage report](https://gitlab.com/adprog-a14/doku/badges/gameAndComment/coverage.svg)](https://gitlab.com/adprog-a14/doku/-/commits/gameAndComment)

#### **[PDF Translate Level](https://gitlab.com/adprog-a14/doku/-/tree/PDF-Translate-Level)**
[![pipeline status](https://gitlab.com/adprog-a14/doku/badges/PDF-Translate-Level/pipeline.svg)](https://gitlab.com/adprog-a14/doku/-/commits/PDF-Translate-Level)
[![coverage report](https://gitlab.com/adprog-a14/doku/badges/PDF-Translate-Level/coverage.svg)](https://gitlab.com/adprog-a14/doku/-/commits/PDF-Translate-Level)

#### **[Overview](https://gitlab.com/adprog-a14/doku/-/tree/Najmi-Overview)**
[![pipeline status](https://gitlab.com/adprog-a14/doku/badges/PDF-Translate-Level/pipeline.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Najmi-Overview)
[![coverage report](https://gitlab.com/adprog-a14/doku/badges/PDF-Translate-Level/coverage.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Najmi-Overview)

#### **[Game Tebak-tebakan](https://gitlab.com/adprog-a14/doku/-/tree/Adil-GameTebakan)**
[![pipeline status](https://gitlab.com/adprog-a14/doku/badges/Adil-GameTebakan/pipeline.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Adil-GameTebakan)
[![coverage report](https://gitlab.com/adprog-a14/doku/badges/Adil-GameTebakan/coverage.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Adil-GameTebakan)

````